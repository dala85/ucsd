from django.urls import path
from .views import qna_history,user_history,vote,chat,users_list,create_chat_room,user_chat,message_admin_help




urlpatterns = [
	
    path('', qna_history,name='qna_history'),
    path('chat/', chat,name='chat'),
    path('users-list/', users_list,name='users_list'),
    path('history/', user_history,name='user_history'),
    path('vote/<int:answer_obj>', vote, name='vote'),
    path('create_chat_room/<str:username>', create_chat_room, name='create_chat_room'),
    path('user_chat/<int:chat_id>', user_chat, name='user_chat'),
    path('message_admin_help/', message_admin_help, name='message_admin_help'),
	
]