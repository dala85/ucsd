from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

class Question(models.Model):
	user = models.ForeignKey(User,on_delete=models.CASCADE)
	question = models.TextField(max_length=1000)
	date = models.DateTimeField(default=timezone.now)

	def write(self):
		self.date = timezone.now()
		self.save()

	def __str__(self):
		return f'{self.question}'



class Answer(models.Model):
	question = models.ForeignKey('qna.question',on_delete=models.CASCADE,related_name='answers')
	admin_answer = models.TextField(max_length=1000)
	date = models.DateTimeField(default=timezone.now)
	likes = models.ManyToManyField(User,related_name='answer_post',blank=True)



	def total_likes(self):
		return self.likes.count()

	def add_answer(self):
		self.date = timezone.now()
		self.save() 

	def __str__(self):
		return f'{self.admin_answer}'


class Chat(models.Model):
	user = models.ForeignKey(User,on_delete=models.CASCADE)
	message = models.TextField(max_length=1000)
	date = models.DateTimeField(default=timezone.now)
	likes = models.ManyToManyField(User,related_name='chat_post',blank=True)


	def write(self):
		self.date = timezone.now()
		self.save()

	def __str__(self):
		return f'{self.message}'

	def total_likes(self):
		return self.likes.count()


class ChatRoom(models.Model):
	name = models.CharField(max_length=100,unique=True)
	participant = models.ManyToManyField(User,related_name='user_chatroom',blank=True)
 

	def __str__(self):
		return f'{self.name}'




class Message(models.Model):
	text = models.CharField(max_length=500)
	user = models.ForeignKey(User,on_delete=models.CASCADE,related_name='author_chatroom')
	chatroom = models.ForeignKey(ChatRoom,on_delete=models.CASCADE,related_name='messages')
	date = models.DateTimeField(default=timezone.now)

	def __str__(self):
		return f'{self.text}'




class MessageAdmin(models.Model):
	user = models.ForeignKey(User,on_delete=models.CASCADE,blank=True,null=True)
	email = models.CharField(max_length=500,blank=True,null=True)
	message = models.TextField(max_length=1000,blank=True,null=True)











