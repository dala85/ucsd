## Project title
TA Bot for UCSD Extension Course "Data Science for Healthcare"


## Code style

[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg?style=flat)](https://github.com/feross/standard)
 
## Screenshots


## Tech/framework used


<b>Built with</b>
- [Sublime](https://www.sublimetext.com)
- [Django](https://docs.djangoproject.com/en/3.0/)



## Installation
1- Create virtual enviorment - > pipenv install 
2- python3 manage.py makemigrations 
3- python3 manage.py migrate 


## Tests
Tests are run on Gitlab 

## How to use?
run python mangage.py makemigrations 
run python manage.py migrate 
run python manage.py createsuperuser


## License


MIT © [Mohammed Ali]()
